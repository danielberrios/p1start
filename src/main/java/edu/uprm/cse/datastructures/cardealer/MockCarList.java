package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class MockCarList {
	private static final CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	static {


	}
	private MockCarList(){}

	public static CircularSortedDoublyLinkedList<Car> getinstance(){

		return carList;
	}

	public static void resetCars() {
		carList.clear();
		
	}
}