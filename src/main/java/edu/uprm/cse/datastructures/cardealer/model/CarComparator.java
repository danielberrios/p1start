package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	@Override
	public int compare(Car o1, Car o2) {
//				String car1 = o1.getCarBrand()+ "-" + o1.getCarModel() + "-" + o1.getCarModelOption();
//				String car2 = o2.getCarBrand()+ "-" + o2.getCarModel() + "-" + o2.getCarModelOption();
//		
//				return car1.compareTo(car2);


		String brandCar1= o1.getCarBrand();
		String brandCar2= o2.getCarBrand();
		String modelCar1= o1.getCarModel();
		String modelCar2= o2.getCarModel();
		String optionCar1= o1.getCarModelOption();
		String optionCar2= o2.getCarModelOption();
		
		/* 
		 * each string have a base part of the cars
		 * first if compare if the brands are equals
		 * second if compare if models are equals
		 * third if compare if the options are equals
		 * each of those comparators give back  either -1,0,1 for the cases like 
		 * if they are the same, one is bigger than the other or viceversa.
		 */
		if(brandCar1.equals(brandCar2)){
			if ( modelCar1.equals(modelCar2)) {	
				if(optionCar1.equals(optionCar2)) {
					return 0;
				}
				return optionCar1.compareTo(optionCar2);
			}
			return modelCar1.compareTo(modelCar2);
		}
		return brandCar1.compareTo(brandCar2);
	}
}