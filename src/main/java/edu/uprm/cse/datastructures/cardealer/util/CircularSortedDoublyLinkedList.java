package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;



public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	private Node<E> header;
	private int currentSize;
	private Comparator <E> comp;

	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this.header = new Node<>();
		this.currentSize = 0;
		this.comp=comp;
	}
	@SuppressWarnings("hiding")
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;


		public CircularSortedDoublyLinkedListIterator() {
			nextNode = (Node<E>) header.getNext();
		}


		@Override
		//header because is a circular list
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {

			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}
		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E> ();
	}

	public boolean add(E e) {
		// if a list is empty, it adds the car right after the header
		if (this.isEmpty()) {
			Node<E> firstTemp= new Node<E>(e,header,header);
			header.setNext(firstTemp);
			header.setPrev(firstTemp);
			currentSize++;
			return true;	
		}
		// new nodes to compare, the next with the new car
		Node<E> newNode = new Node<E>(e, null, null);
		Node<E> firstNode = header;


		//we put the elements in variables to be able to compare them 
		E o2 = newNode.getElement();

		/* 
		 * For loop to iterate across the list
		 * compare the elements, if the comparator returns (1) the new element will be located in front of the first node
		 * in that exact moment of the iteration. have in mind that first node goes through the list and do not means explicit first node
		 */
				for(firstNode=this.header.getNext(); firstNode!=header; firstNode= firstNode.getNext()) {
					if(comp.compare(firstNode.getElement(), o2)>0) {
					newNode.setNext(firstNode);
					newNode.setPrev(firstNode.getPrev());
					firstNode.getPrev().setNext(newNode);
					firstNode.setPrev(newNode);
						currentSize++;
						return true;
					}
				}
				/*
				 * If it reach the end of the list and the IF located up here does not return true, then it means
				 * its going to be added at the end of the list
				 */

						newNode.setPrev(header.getPrev());
						newNode.setNext(header);
						header.setPrev(newNode);
						newNode.getPrev().setNext(newNode);
						currentSize++;
						return true;
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);
		if (i < 0) {
			return false;
		}else {
			this.remove(i);
			return true;
		}
	}

	@Override 
	public boolean remove(int index) {
		if ((index < 0) || (index >= this.currentSize)){
			throw new IndexOutOfBoundsException();
		}
		else {
			Node<E> temp = header;
			int currentPosition =0;
			Node<E> target = header;

			while (currentPosition != index) {
				temp = temp.getNext();
				currentPosition++;
			}
			target = temp.getNext();
			temp.setNext(target.getNext());
			target.getNext().setPrev(temp);
			target.setNext(null);
			target.setElement(null);
			currentSize--;
			return true;

		}
	}

	@Override
	public int removeAll(E e) {
		int count = 0;
		while (this.remove(e)) {
			count++;
		}
		return count;
	}


	@Override
	public E first() {
		if (this.isEmpty()) {
			return header.getElement();
		}
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		if (this.isEmpty()) {
			return header.getElement();
		}

		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		int position=0;
		Node<E> temp = this.header.getNext();
		if ((index < 0) || (index >= this.currentSize)){
			throw new IndexOutOfBoundsException();
		}
		else
		{
			while (position != index) {
				temp = temp.getNext();
				position++;
			}
			return temp.getElement();
		}
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			remove(0);
		}
	}

	@Override
	public boolean contains(E e) {

		return firstIndex(e)>0 ? true : false;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;

	}

	@Override
	//return the first position of an object, if not found returns -1
	public int firstIndex(E e) {
		int i = 0;
		for (Node<E> temp = this.header.getNext(); temp != header; 
				temp = temp.getNext(), ++i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	//return the last position of an object, if not found returns -1
	public int lastIndex(E e) {
		int i = 0;
		int lastPosition = -1; 
		for (Node<E> temp = this.header.getNext(); temp != header; 
				temp = temp.getNext(), ++i) {
			if (temp.getElement().equals(e)) {
				lastPosition = i;
			}
		}
		return lastPosition;
	}

}