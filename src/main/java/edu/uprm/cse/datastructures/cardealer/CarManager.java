package edu.uprm.cse.datastructures.cardealer;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {
	private final CircularSortedDoublyLinkedList<Car> carList = MockCarList.getinstance();
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	//Move our car list to an array of cars
	public Car[] getAllCars() {
		Car[] list = new Car[carList.size()];
		int i = 0;
		for(Car c: carList) {
			list[i]=c;
			i++;
		}
		return list;
	}   
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	//search for a expecific car with an id
	public Car getCar(@PathParam("id") long id){
		for(int i=0; i<carList.size();i++) {
			if (carList.get(i).getCarId()==id) {
				return carList.get(i);
			}
		}
		throw new NotFoundException(new JsonError("Error", "Car" + id + " not found"));	
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	//add cars to the list, verify if any car got a the same id or a empty space to not add it 
	public Response addCar(Car cars){
		for(int i=0; i<carList.size();i++) {
			if (carList.get(i).getCarId()==cars.getCarId() || cars.equals(null)) {
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			}
		}
		
		carList.add(cars);
		return Response.status(201).build();
	}  

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	//update the info of the car
	public Response updateCar(Car cars, @PathParam("id") int id){

		for(Car c : carList) {
			if(c.getCarId() == id) {
				carList.remove(c);
				carList.add(cars);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();      
	} 
	@DELETE
	@Path("/{id}/delete")
	//remove a car from the list
	public Response deleteCar(@PathParam("id") long id){
		for (Car c: carList) {
			if(c.getCarId() == id) {
				carList.remove(c);
				return Response.status(200).build();
			}
		}
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	}      
}